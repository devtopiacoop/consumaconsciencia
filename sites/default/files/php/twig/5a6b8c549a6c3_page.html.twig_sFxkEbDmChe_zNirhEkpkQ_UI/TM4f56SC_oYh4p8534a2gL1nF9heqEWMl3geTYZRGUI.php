<?php

/* themes/consuma/templates/page.html.twig */
class __TwigTemplate_d78b8ccf35cdfa3de9de35b5b6d5871ade300ee562e4e6e7311007e6881b6169 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navbar' => array($this, 'block_navbar'),
            'content_top' => array($this, 'block_content_top'),
            'main' => array($this, 'block_main'),
            'header' => array($this, 'block_header'),
            'sidebar_first' => array($this, 'block_sidebar_first'),
            'highlighted' => array($this, 'block_highlighted'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'sidebar_second' => array($this, 'block_sidebar_second'),
            'footer' => array($this, 'block_footer'),
            'footer_left' => array($this, 'block_footer_left'),
            'footer_center' => array($this, 'block_footer_center'),
            'footer_right' => array($this, 'block_footer_right'),
            'footer_bottom' => array($this, 'block_footer_bottom'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 54, "if" => 56, "block" => 57);
        $filters = array("clean_class" => 62, "t" => 74);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('clean_class', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 54
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "fluid_container", array())) ? ("container-fluid") : ("container"));
        // line 56
        if (($this->getAttribute(($context["page"] ?? null), "navigation", array()) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()))) {
            // line 57
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 94
        echo "
";
        // line 95
        if ($this->getAttribute(($context["page"] ?? null), "content_top", array())) {
            // line 96
            $this->displayBlock('content_top', $context, $blocks);
        }
        // line 104
        echo "
";
        // line 106
        $this->displayBlock('main', $context, $blocks);
        // line 171
        echo "
";
        // line 172
        if ($this->getAttribute(($context["page"] ?? null), "footer", array())) {
            // line 173
            $this->displayBlock('footer', $context, $blocks);
        }
        // line 204
        echo "
";
        // line 205
        if ($this->getAttribute(($context["page"] ?? null), "footer_bottom", array())) {
            // line 206
            $this->displayBlock('footer_bottom', $context, $blocks);
        }
    }

    // line 57
    public function block_navbar($context, array $blocks = array())
    {
        // line 59
        $context["navbar_classes"] = array(0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 61
($context["theme"] ?? null), "settings", array()), "navbar_inverse", array())) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 62
($context["theme"] ?? null), "settings", array()), "navbar_position", array())) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "navbar_position", array())))) : (($context["container"] ?? null))));
        // line 65
        echo "<header";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["navbar_attributes"] ?? null), "addClass", array(0 => ($context["navbar_classes"] ?? null)), "method"), "html", null, true));
        echo " id=\"navbar\" role=\"banner\">
";
        // line 66
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", array(0 => ($context["container"] ?? null)), "method")) {
            // line 67
            echo "<div class=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
            echo "\">
  ";
        }
        // line 69
        echo "  <div class=\"navbar-header\">
    ";
        // line 70
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation", array()), "html", null, true));
        echo "
    ";
        // line 72
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 73
            echo "    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
      <span class=\"sr-only\">";
            // line 74
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation")));
            echo "</span>
      <span class=\"icon-bar\"></span>
      <span class=\"icon-bar\"></span>
      <span class=\"icon-bar\"></span>
    </button>
    ";
        }
        // line 80
        echo "  </div>

  ";
        // line 83
        echo "  ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 84
            echo "  <div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
    ";
            // line 85
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()), "html", null, true));
            echo "
  </div>
  ";
        }
        // line 88
        echo "  ";
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", array(0 => ($context["container"] ?? null)), "method")) {
            // line 89
            echo "</div>
";
        }
        // line 91
        echo "</header>
";
    }

    // line 96
    public function block_content_top($context, array $blocks = array())
    {
        // line 97
        echo "<div class=\"content-top\">
  <div class=\"content-top-wrapper\">
    ";
        // line 99
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content_top", array()), "html", null, true));
        echo "
  </div>
</div>
";
    }

    // line 106
    public function block_main($context, array $blocks = array())
    {
        // line 107
        echo "<div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
        echo " js-quickedit-main-content\">
  <div class=\"row\">

    ";
        // line 111
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 112
            echo "    ";
            $this->displayBlock('header', $context, $blocks);
            // line 117
            echo "    ";
        }
        // line 118
        echo "
    ";
        // line 120
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())) {
            // line 121
            echo "    ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 126
            echo "    ";
        }
        // line 127
        echo "
    ";
        // line 129
        echo "    ";
        // line 130
        $context["content_classes"] = array(0 => ((($this->getAttribute(        // line 131
($context["page"] ?? null), "sidebar_first", array()) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 132
($context["page"] ?? null), "sidebar_first", array()) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 133
($context["page"] ?? null), "sidebar_second", array()) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 134
($context["page"] ?? null), "sidebar_first", array())) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())))) ? ("col-sm-12") : ("")));
        // line 137
        echo "    <section";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content_attributes"] ?? null), "addClass", array(0 => ($context["content_classes"] ?? null)), "method"), "html", null, true));
        echo ">

    ";
        // line 140
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", array())) {
            // line 141
            echo "    ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 144
            echo "    ";
        }
        // line 145
        echo "
    ";
        // line 147
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "help", array())) {
            // line 148
            echo "    ";
            $this->displayBlock('help', $context, $blocks);
            // line 151
            echo "    ";
        }
        // line 152
        echo "
    ";
        // line 154
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
        // line 158
        echo "    </section>

    ";
        // line 161
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())) {
            // line 162
            echo "    ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 167
            echo "    ";
        }
        // line 168
        echo "  </div>
</div>
";
    }

    // line 112
    public function block_header($context, array $blocks = array())
    {
        // line 113
        echo "    <div class=\"col-sm-12\" role=\"heading\">
      ";
        // line 114
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
        echo "
    </div>
    ";
    }

    // line 121
    public function block_sidebar_first($context, array $blocks = array())
    {
        // line 122
        echo "    <aside class=\"col-sm-3\" role=\"complementary\">
      ";
        // line 123
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_first", array()), "html", null, true));
        echo "
    </aside>
    ";
    }

    // line 141
    public function block_highlighted($context, array $blocks = array())
    {
        // line 142
        echo "    <div class=\"highlighted\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "highlighted", array()), "html", null, true));
        echo "</div>
    ";
    }

    // line 148
    public function block_help($context, array $blocks = array())
    {
        // line 149
        echo "    ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "help", array()), "html", null, true));
        echo "
    ";
    }

    // line 154
    public function block_content($context, array $blocks = array())
    {
        // line 155
        echo "    <a id=\"main-content\"></a>
    ";
        // line 156
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
        echo "
    ";
    }

    // line 162
    public function block_sidebar_second($context, array $blocks = array())
    {
        // line 163
        echo "    <aside class=\"col-sm-3\" role=\"complementary\">
      ";
        // line 164
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()), "html", null, true));
        echo "
    </aside>
    ";
    }

    // line 173
    public function block_footer($context, array $blocks = array())
    {
        // line 174
        echo "<footer class=\"footer \" role=\"contentinfo\">
  <div class=\"footer-wrapper ";
        // line 175
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
        echo "\">
    ";
        // line 176
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer", array()), "html", null, true));
        echo "
    <div class=\"row\">
      <div class=\"col-md-4\">
        ";
        // line 179
        if ($this->getAttribute(($context["page"] ?? null), "footer_left", array())) {
            // line 180
            echo "        ";
            $this->displayBlock('footer_left', $context, $blocks);
            // line 183
            echo "        ";
        }
        // line 184
        echo "      </div>
      <div class=\"col-md-4\">
        ";
        // line 186
        if ($this->getAttribute(($context["page"] ?? null), "footer_center", array())) {
            // line 187
            echo "        ";
            $this->displayBlock('footer_center', $context, $blocks);
            // line 190
            echo "        ";
        }
        // line 191
        echo "      </div>
      <div class=\"col-md-4\">
        ";
        // line 193
        if ($this->getAttribute(($context["page"] ?? null), "footer_right", array())) {
            // line 194
            echo "        ";
            $this->displayBlock('footer_right', $context, $blocks);
            // line 197
            echo "        ";
        }
        // line 198
        echo "      </div>
    </div>
  </div>
</footer>
";
    }

    // line 180
    public function block_footer_left($context, array $blocks = array())
    {
        // line 181
        echo "          ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_left", array()), "html", null, true));
        echo "
        ";
    }

    // line 187
    public function block_footer_center($context, array $blocks = array())
    {
        // line 188
        echo "          ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_center", array()), "html", null, true));
        echo "
        ";
    }

    // line 194
    public function block_footer_right($context, array $blocks = array())
    {
        // line 195
        echo "          ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_right", array()), "html", null, true));
        echo "
        ";
    }

    // line 206
    public function block_footer_bottom($context, array $blocks = array())
    {
        // line 207
        echo "<div id=\"footer-bottom\" class=\"\" role=\"contentinfo\">
  <div class=\"footer-wrapper ";
        // line 208
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["container"] ?? null), "html", null, true));
        echo "\">
    ";
        // line 209
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_bottom", array()), "html", null, true));
        echo "
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/consuma/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  441 => 209,  437 => 208,  434 => 207,  431 => 206,  424 => 195,  421 => 194,  414 => 188,  411 => 187,  404 => 181,  401 => 180,  393 => 198,  390 => 197,  387 => 194,  385 => 193,  381 => 191,  378 => 190,  375 => 187,  373 => 186,  369 => 184,  366 => 183,  363 => 180,  361 => 179,  355 => 176,  351 => 175,  348 => 174,  345 => 173,  338 => 164,  335 => 163,  332 => 162,  326 => 156,  323 => 155,  320 => 154,  313 => 149,  310 => 148,  303 => 142,  300 => 141,  293 => 123,  290 => 122,  287 => 121,  280 => 114,  277 => 113,  274 => 112,  268 => 168,  265 => 167,  262 => 162,  259 => 161,  255 => 158,  252 => 154,  249 => 152,  246 => 151,  243 => 148,  240 => 147,  237 => 145,  234 => 144,  231 => 141,  228 => 140,  222 => 137,  220 => 134,  219 => 133,  218 => 132,  217 => 131,  216 => 130,  214 => 129,  211 => 127,  208 => 126,  205 => 121,  202 => 120,  199 => 118,  196 => 117,  193 => 112,  190 => 111,  183 => 107,  180 => 106,  172 => 99,  168 => 97,  165 => 96,  160 => 91,  156 => 89,  153 => 88,  147 => 85,  144 => 84,  141 => 83,  137 => 80,  128 => 74,  125 => 73,  122 => 72,  118 => 70,  115 => 69,  109 => 67,  107 => 66,  102 => 65,  100 => 62,  99 => 61,  98 => 59,  95 => 57,  90 => 206,  88 => 205,  85 => 204,  82 => 173,  80 => 172,  77 => 171,  75 => 106,  72 => 104,  69 => 96,  67 => 95,  64 => 94,  61 => 57,  59 => 56,  57 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/consuma/templates/page.html.twig", "/home/consumac/public_html/themes/consuma/templates/page.html.twig");
    }
}
