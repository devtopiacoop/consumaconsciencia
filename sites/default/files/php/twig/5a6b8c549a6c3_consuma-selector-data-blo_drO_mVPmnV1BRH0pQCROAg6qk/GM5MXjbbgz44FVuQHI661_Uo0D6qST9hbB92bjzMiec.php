<?php

/* modules/custom/consumaconsciencia/templates/consuma-selector-data-block.html.twig */
class __TwigTemplate_70e4af5f0fbc77d7b80fed570c9f3af7c181d20cfc1ba8ffbfe22b9d81a5a1d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("for" => 12);
        $filters = array("t" => 6);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('for'),
                array('t'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div id=\"consuma-selector\">
    <div class=\"row\">
        <div class=\"col-md-4\">
            <div id=\"selector-conocer-entidad\" class=\"selector-block\">
                <div class=\"selector-intro\">
                    <h3>";
        // line 6
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Conocer entidades")));
        echo "</h3>
                </div>
                <div class=\"selector-wrapper\">
                    <label for=\"selector-entidad\">";
        // line 9
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Entidad:")));
        echo "</label>
                    <select id=\"selector-entidad\" class=\"selector-unico\">
                        <option class=\"item\" value=\"\">";
        // line 11
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("-Selecciona una entidad-")));
        echo "</option>
                        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["entities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 13
            echo "                            <option class=\"item\" value=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true));
            echo "\">
                                ";
            // line 14
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "title", array()), "value", array()), "html", null, true));
            echo "
                            </option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "                    </select>
                </div>
                <a href=\"\" class=\"action-button btn-consuma\"  title=\"Accede a conocer la entidad\" />";
        // line 19
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Ver")));
        echo "</a>
            </div>
        </div>
        <div class=\"col-md-4\">
            <div id=\"selector-comparar-entidades\" class=\"selector-block\">
                <div class=\"selector-intro\">
                    <h3>";
        // line 25
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Comparar entidades")));
        echo "</h3>
                </div>
                <div class=\"selector-wrapper\">
                    <label for=\"selector-entidad-uno\">";
        // line 28
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Entidad 1:")));
        echo "</label>
                    <select id=\"selector-entidad-uno\" class=\"selector-doble uno\">
                        <option class=\"item\" value=\"\">";
        // line 30
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("-Selecciona una entidad-")));
        echo "</option>
                        ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["entities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 32
            echo "                            <option class=\"item\" value=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true));
            echo "\">
                                ";
            // line 33
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "title", array()), "value", array()), "html", null, true));
            echo "
                            </option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "                    </select>
                    <label for=\"selector-entidad-uno\">";
        // line 37
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Entidad 2:")));
        echo "</label>
                    <select id=\"selector-entidad-dos\" class=\"selector-doble dos\">
                        <option class=\"item\" value=\"\">";
        // line 39
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("-Selecciona una entidad-")));
        echo "</option>
                        ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["entities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 41
            echo "                            <option class=\"item\" value=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true));
            echo "\">
                                ";
            // line 42
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "title", array()), "value", array()), "html", null, true));
            echo "
                            </option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                    </select>
                </div>
                <a href=\"\" class=\"action-button btn-consuma\"  title=\"Accede a comparar las entidades\" />";
        // line 47
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Ver")));
        echo "</a>
            </div>
        </div>
        <div class=\"col-md-4\">
            <div id=\"selector-comparar-detalles\" class=\"selector-block\">
                <div class=\"selector-intro\">
                    <h3>";
        // line 53
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Comparar detalles")));
        echo "</h3>
                </div>
                <div class=\"selector-wrapper\">
                    <label for=\"selector-indicador\">";
        // line 56
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Grupo:")));
        echo "</label>
                    <select id=\"selector-indicador\" class=\"\">
                        <option class=\"item\" value=\"\">";
        // line 58
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("-Selecciona un grupo-")));
        echo "</option>
                        <option class=\"item\" value=\"aspectos-descriptivos\">";
        // line 59
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Aspectos descriptivos")));
        echo "</option>
                        <option class=\"item\" value=\"volumen\">";
        // line 60
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Volumen")));
        echo "</option>
                        <option class=\"item\" value=\"producto-servicio\">";
        // line 61
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Producto o servicio")));
        echo "</option>
                        <option class=\"item\" value=\"produccion-energia\">";
        // line 62
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Producción energía")));
        echo "</option>
                        <option class=\"item\" value=\"gobernanza-interna\">";
        // line 63
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Gobernanza interna")));
        echo "</option>
                        <option class=\"item\" value=\"gestion-economica\">";
        // line 64
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Gestión económica")));
        echo "</option>
                        <option class=\"item\" value=\"comunidad\">";
        // line 65
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Comunidad")));
        echo "</option>
                        <option class=\"item\" value=\"medio-ambiente\">";
        // line 66
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Medio Ambiente")));
        echo "</option>
                    </select>
                </div>
                <a href=\"\" class=\"action-button btn-consuma\"  title=\"Accede a comparar por indicadores\" />";
        // line 69
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Ver")));
        echo "</a>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/consumaconsciencia/templates/consuma-selector-data-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 69,  215 => 66,  211 => 65,  207 => 64,  203 => 63,  199 => 62,  195 => 61,  191 => 60,  187 => 59,  183 => 58,  178 => 56,  172 => 53,  163 => 47,  159 => 45,  150 => 42,  145 => 41,  141 => 40,  137 => 39,  132 => 37,  129 => 36,  120 => 33,  115 => 32,  111 => 31,  107 => 30,  102 => 28,  96 => 25,  87 => 19,  83 => 17,  74 => 14,  69 => 13,  65 => 12,  61 => 11,  56 => 9,  50 => 6,  43 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "modules/custom/consumaconsciencia/templates/consuma-selector-data-block.html.twig", "/home/consumac/public_html/modules/custom/consumaconsciencia/templates/consuma-selector-data-block.html.twig");
    }
}
