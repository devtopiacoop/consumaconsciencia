<?php

namespace Drupal\consumaconsciencia\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;

/**
 * Provides a 'ConsumaSelectorDataBlock' block.
 *
 * @Block(
 *  id = "consuma_selector_data_block",
 *  admin_label = @Translation("Consuma selector data block"),
 * )
 */
class ConsumaSelectorDataBlock extends BlockBase
{

    /**
     * {@inheritdoc}
     */
    public function build()
    {

        $loggedUser = \Drupal::currentUser();

        $entities = [];

        $entitiesNids = \Drupal::entityQuery('node')
            ->condition('type', 'entity')
            ->condition('status', 1)
        //->condition('uid',$loggedUser->id())
            ->sort('title', 'ASC')
            ->execute();

        $entities = Node::loadMultiple($entitiesNids);

        $build = [
            '#theme' => 'consuma_selector_data_block',
            '#attached' => [
                'library' => ['consumaconsciencia/consumaconsciencia.entities_selector'],
            ],
            '#entities' => $entities,
        ];

        return $build;
    }

}
