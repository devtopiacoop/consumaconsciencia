(function ($) {
    //
    // First block behavior: update href on change option
    //
    $("#selector-conocer-entidad select.selector-unico").change(function(){
        $("#selector-conocer-entidad a.action-button").attr("href","node/"+$(this).val());
    });

    //
    // Second block behavior: update href on change option
    //
    $("#selector-comparar-entidades select.selector-doble").change(function(){
        var _href = "entidades-comparativa/" +
            $("#selector-comparar-entidades select.selector-doble.uno option:selected").val() +
            "," +
            $("#selector-comparar-entidades select.selector-doble.dos option:selected").val();

        $("#selector-comparar-entidades a.action-button").attr("href",_href);
    });
    // Unset option from the other selector
    $("#selector-comparar-entidades select.selector-doble.uno").change(function(){
        // Restore last selection
        $("#selector-comparar-entidades select.selector-doble.dos option[value='"+$(this).attr("selid")+"']").show();
        // Remove
        $("#selector-comparar-entidades select.selector-doble.dos option[value='"+$(this).val()+"']").hide();
        // Save as attr
        $(this).attr("selid",$(this).val());
    });
    // Unset option from the other selector
    $("#selector-comparar-detalles select").change(function(){
        // Set href
        $("#selector-comparar-detalles a.action-button").attr("href","comparativa-indicadores/"+$(this).val());
    });

    //
    // Third block behavior
    //

})(jQuery)