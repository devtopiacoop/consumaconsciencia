(function ($) {
    function calculateDivision(op1,op2,result) {
        if ( !isNaN(op1) && !isNaN(op2)) {
        result.val((op1/op2).toFixed(2));
        }
    }

    // Inversiones en I+D+i (inversiones/número de clientes)
        inversion_result = $('#edit-field-ge-iv-a-0-value');
        inversion_op1 = $('#edit-field-ge-iv-pre-0-value');
        inversion_op2 = $('#edit-field-de-xiii-0-value');

        inversion_op1.change(function(){
            calculateDivision(inversion_op1.val(),inversion_op2.val(),inversion_result);
        });
        inversion_op2.change(function(){
            calculateDivision(inversion_op1.val(),inversion_op2.val(),inversion_result);
        });

    // Beneficio total/número de clientes totales
        benefit_result = $('#edit-field-ge-vii-0-value');
        benefit_op1 = $('#edit-field-de-vii-0-value');
        benefit_op2 = $('#edit-field-de-xiii-0-value');

        benefit_op1.change(function(){
            calculateDivision(benefit_op1.val(),benefit_op2.val(),benefit_result);
        });
        benefit_op2.change(function(){
            calculateDivision(benefit_op1.val(),benefit_op2.val(),benefit_result);
        });

    // Beneficio en España/número de clientes en España
        benefit_sp_result = $('#edit-field-ge-viii-0-value');
        benefit_sp_op1 = $('#edit-field-de-viii-0-value');
        benefit_sp_op2 = $('#edit-field-de-xiv-0-value');

        benefit_sp_op1.change(function(){
            calculateDivision(benefit_sp_op1.val(),benefit_sp_op2.val(),benefit_sp_result);
        });
        benefit_sp_op2.change(function(){
            calculateDivision(benefit_sp_op1.val(),benefit_sp_op2.val(),benefit_sp_result);
        });

    // Acción social (cantidad anual dedicada a acción social/número de clientes)
        social_result = $('#edit-field-ge-v-a-0-value');
        social_op1 = $('#edit-field-ge-v-pre-0-value');
        social_op2 = $('#edit-field-de-xiii-0-value');

        social_op1.change(function(){
            calculateDivision(social_op1.val(),social_op2.val(),social_result);
        });
        social_op2.change(function(){
            calculateDivision(social_op1.val(),social_op2.val(),social_result);
        });

   
})(jQuery)
