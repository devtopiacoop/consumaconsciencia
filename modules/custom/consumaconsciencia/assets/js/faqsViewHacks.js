(function ($) {
    // Toggle faqs
    $( ".view-preguntas-frecuentes .field--name-body" ).hide();
    $( ".view-preguntas-frecuentes .field--name-node-title h2" ).click(function() {
      $(this).parent().next().slideToggle( "slow" );
    });

})(jQuery)