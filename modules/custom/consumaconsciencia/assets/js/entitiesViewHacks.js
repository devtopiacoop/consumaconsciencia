(function ($) {
    // Show label description
    $('.field > .field--label').click(function(e){
        // Hide all descriptions
        $('.field--label .label-description').hide();
        // Toggle
        if ( $(this).find('.label-description').hasClass("visible") ) {
            $(this).find('.label-description').fadeOut("slow").removeClass("visible");
        } else{
            $(this).find('.label-description').fadeIn("slow").addClass("visible");
        }

    });
    // Show field info
    $('.field > .field--item').click(function(e){
        // Hide all
        $('.field .node--type-info-indicador').hide();

        // Toggle
        var _info = $(this).parent().next().find('.node--type-info-indicador');
        if ( _info.hasClass("visible") ) {
            _info.fadeOut("slow").removeClass("visible");
        } else{
            _info.fadeIn("slow").addClass("visible");
        }
    });

    // Close info window
    $('.node--type-entity.node--view-mode-full .panel .panel-body .field .node--type-info-indicador').click(function(){
       $(this).hide();
    });

    // Popover
    $(function(){
        // Clean those which has not info box
        $('[rel="popover"]').each(function(){
            _info_box=$(this).data('popover-content');
            if (!$(_info_box).length) {
                $(this).remove();
            }
        });


        $('[rel="popover"]').popover({
            container: 'body',
            html: true,
            content: function () {
                var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
                return clone;
            }
        }).click(function(e) {
            e.preventDefault();
        });
    });

})(jQuery)